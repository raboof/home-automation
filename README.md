This is a NixOS spec for a Raspberry Pi home automation component.

# Goal

My house has a central heating system with a thermostat that
sits in the living room. Often, however, we only use the office.
The office does have a radiator, but if the living room is
already at the desired temperature, the central heating does
not kick in and the room remains cold.

# Approach

The original idea was that `mqtt2relay` would call the HTTP
URI of the relay that's attached to my central heating, to
enable it whenever heat is required.

However, I since figured this can be achieved with the 'actions'
feature of TRV itself, removing the need for the Raspberry Pi
entirely :D.

So I'm not finishing/using the project in this repo for now.
In the future perhaps I'll revisit this: the RPi can do smarter
things, keep history and perhaps perform some other tasks, and
it can replace the ESP32 that is currently driving the relay
attached to the central heating. The immediate need for that is
gone, though :).

# Parts

The image sets up an MQTT broker and installs an `mqtt2relay`
script that listens to the topic that is populated by my
[Shelly TRV](https://shop.shelly.cloud/shelly-trv-wifi-smart-home-automation#597).

# Building

It's a Raspberry Pi 3 Model B+. Build the sd card image with:

  unzstd $(nixos-generate --system aarch64-linux -f sd-aarch64 -c configuration.nix) -o generated.img

And then put it on the card with:

  dd if=generated.img of=/dev/mmcblk0 bs=4M

(or whereever your sd card is)
