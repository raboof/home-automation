{ config, lib, pkgs, ... }:
{
  #services.sshd.enable = true;
  #services.nginx.enable = true;

  documentation.enable = false;

  networking.hostName = "ha";
  networking.firewall.allowedTCPPorts = [
    1883
    80
  ];
  networking.wireless = {
    enable = true;
    networks = import ./wifi-config.nix;
  };

  hardware.bluetooth.enable = false;
  
  services.mosquitto = {
    enable = true;
    listeners = [ {
        acl = [ "pattern readwrite #" ];
        omitPasswordAuth = true;
        settings.allow_anonymous = true;
    } ];
  };
  
  #users.users.root.password = "nixos";
  #services.openssh.permitRootLogin = lib.mkDefault "yes";
  services.getty.autologinUser = lib.mkDefault "root";

  environment.systemPackages = [
    (pkgs.callPackage mqtt2relay/default.nix {})
  ];
}
