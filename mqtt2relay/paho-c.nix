{ stdenv
, fetchFromGitHub
, cmake
}:

stdenv.mkDerivation rec {
  pname = "paho.mqtt.c";
  version = "1.3.9";

  src = fetchFromGitHub {
    owner = "eclipse";
    repo = "paho.mqtt.c";
    rev = "v${version}";
    sha256 = "sha256-/Hom8nCUyLeOjf91b0l+La9fUUurawWn0V0bt5ylRHM=";
  };

  nativeBuildInputs = [ cmake ];
  cmakeFlags = [ "-DPAHO_WITH_SSL=FALSE" ];
}
