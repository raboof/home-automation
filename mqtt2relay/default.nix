{ lib
, rustPlatform
, callPackage }:

rustPlatform.buildRustPackage rec {
  pname = "mqtt2relay";
  version = "snapshot";

  buildInputs = [
    (callPackage ./paho-c.nix {})
  ];

  src = ./.;

  cargoSha256 = "sha256-Plfb09c98sO+Tv2qks6+x/qPMvPqD+7J7vQUdbtlixI=";
}
