extern crate paho_mqtt;
use std::{
  process,
  time::Duration
};

fn subscribe_topics(client: &paho_mqtt::Client) {
  if let Err(e) = client.subscribe("shellies/shellytrv-60A423D90EAA/info", 0) {
    println!("Error subscribing: {:?}", e);
    process::exit(1);
  }
}

fn main() {
  let create_opts = paho_mqtt::CreateOptionsBuilder::new()
    .server_uri("tcp://ha:1883")
    .client_id("mqtt2relay")
    .finalize();
  
  let mut client = paho_mqtt::Client::new(create_opts).unwrap_or_else(|err| {
    println!("Error creating the client: {:?}", err);
    process::exit(1);
  });

  let rx = client.start_consuming();
  let last_will = paho_mqtt::MessageBuilder::new()
    .topic("mqtt2relay")
    .payload("lost connection")
    .finalize();
  let conn_opts = paho_mqtt::ConnectOptionsBuilder::new()
    .keep_alive_interval(Duration::from_secs(20))
    .clean_session(false)
    .will_message(last_will)
    .finalize();
  if let Err(e) = client.connect(conn_opts) {
    println!("Unable to connect: {:?}", e);
    process::exit(1);
  }
  subscribe_topics(&client);
  for msg in rx.iter() {
    if let Some(msg) = msg {
      println!("{}", msg);
    } else if !client.is_connected() {
    }
  }
}
